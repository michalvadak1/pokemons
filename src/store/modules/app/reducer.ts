import { createSlice } from '@reduxjs/toolkit'
import { AppStateInterface } from '../../../interfaces'

const initialState: AppStateInterface = {
  recentlyVisited: [],
  favourites: [],
}

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    addRecentlyVisited(state, action) {
      const found = state.recentlyVisited?.findIndex(value => value.id === action.payload.id)

      if (found !== -1) {
        state.recentlyVisited.splice(found, 1)
      }

      state.recentlyVisited.push(action.payload)
      if (state.recentlyVisited.length > 4) {
        state.recentlyVisited.shift()
      }
    },
    addFavourite(state, action) {
      state.favourites.push(action.payload)
    },
    removeFavourite(state, action) {
      const found = state.favourites?.findIndex(value => value.id === action.payload.id)

      if (found !== -1) {
        state.favourites.splice(found, 1)
      }
    },
  },
})

const { actions, reducer } = appSlice
export const { addFavourite, addRecentlyVisited, removeFavourite } = actions
export const appReducer = reducer
