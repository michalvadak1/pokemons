import { createApi } from '@reduxjs/toolkit/query/react'
import {
  GetPokemonInterface,
  GetPokemonResponseInterface,
  GetPokemonsInterface,
  GetPokemonsResponseInterface,
} from '../../../interfaces'
import { baseQuery } from '../../../services/fetch'

export const appApi = createApi({
  reducerPath: 'appApi',
  baseQuery: baseQuery,
  endpoints: builder => ({
    getPokemons: builder.query<GetPokemonsResponseInterface, GetPokemonsInterface>({
      query: params => {
        return {
          url: '/v2/pokemon',
          method: 'GET',
          params,
        }
      },
    }),
    getPokemon: builder.query<GetPokemonResponseInterface, GetPokemonInterface>({
      query: ({ id }) => {
        return {
          url: `/v2/pokemon/${id}`,
          method: 'GET',
        }
      },
    }),
  }),
})

export const { useGetPokemonsQuery, useGetPokemonQuery } = appApi
