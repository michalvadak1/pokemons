export interface GetPokemonsInterface {
  limit: number
  offset: number
}

export interface GetPokemonsResponseInterface {
  count: number
  next: string | null
  previous: string | null
  results: Pokemon[]
}

export interface GetPokemonInterface {
  id?: string
}

export interface GetPokemonResponseInterface {
  id: number
  name: string
  height: number
  weight: number
  sprites: {
    front_default: string
    back_default: string

    front_shiny: string
    back_shiny: string
  }
}

export interface Pokemon {
  name: string
  url: string
}
