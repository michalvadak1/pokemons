export interface AppStateInterface {
  recentlyVisited: Pokemon[]
  favourites: Pokemon[]
}

interface Pokemon {
  name: string
  id: string
}
