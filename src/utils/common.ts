export function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(' ')
}

export function showError(touched: boolean, error?: string) {
  if (touched) {
    return error
  }
  return null
}
