import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Pokemons from './views/pokemons/Pokemons'
import PokemonDetail from './views/pokemon-detail/PokemonDetail'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Pokemons />} />
        <Route path="/:id" element={<PokemonDetail />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
