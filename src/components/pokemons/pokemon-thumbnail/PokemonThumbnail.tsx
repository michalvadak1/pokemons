import React from 'react'
import { useGetPokemonQuery } from '../../../store'
import { Link } from 'react-router-dom'

interface PokemonThumbnailProps {
  name: string
  id: string
}

export default function PokemonThumbnail(props: PokemonThumbnailProps) {
  const { name, id } = props

  const { data: pokemonData } = useGetPokemonQuery({ id })

  return (
    <Link to={`/${id}`} className="flex flex-col col-span-3 justify-center items-center">
      <img src={pokemonData?.sprites.front_default} alt="" />
      <span className="first-letter:uppercase">{name}</span>
    </Link>
  )
}
