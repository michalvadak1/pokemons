import { Form, Formik, FormikHelpers } from 'formik'
import FormInput from '../../common/form-inputs/FormInput'
import React from 'react'
import { useNavigate } from 'react-router-dom'

interface formValuesInterface {
  name: string
}

export default function GoToPokemonForm() {
  const navigate = useNavigate()

  const submitHandler = (values: formValuesInterface, formikHelpers: FormikHelpers<formValuesInterface>) => {
    formikHelpers.setSubmitting(false)
    console.log(values)
    navigate(`/${values.name}`)
  }

  const initialValues: formValuesInterface = {
    name: '',
  }

  return (
    <div className="flex justify-center">
      <Formik initialValues={initialValues} onSubmit={submitHandler}>
        <Form className="grid grid-cols-12 gap-3 w-60">
          <FormInput className="col-span-12" label="Go to pokemon detail:" type="text" name="name" />
          <div className="col-span-12 text-center">
            <button
              type="submit"
              id="go-to-pokemon-form-submit"
              className="inline-flex items-center py-2 px-4 text-sm font-medium rounded-md border border-transparent focus:outline-none focus:ring-2 focus:ring-offset-2 shadow-sm text-secondary bg-primary hover:bg-primary-dark focus:ring-primary"
            >
              Go
            </button>
          </div>
        </Form>
      </Formik>
    </div>
  )
}
