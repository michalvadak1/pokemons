import { Field, useField } from 'formik'
import { classNames, showError } from '../../../utils/common'

interface FormInputInterface {
  className: string
  label: string
  type: 'text' | 'number' | 'email' | 'password'
  name: string
  placeholder?: string
  hideLabel?: boolean
}

export default function FormInput(props: FormInputInterface) {
  const { className, label, type, name, placeholder, hideLabel } = props
  const [, { error, touched }] = useField(name)

  return (
    <div className={classNames(className)}>
      <label
        htmlFor={name}
        className={classNames('block text-sm font-medium text-gray-700', hideLabel ? 'sr-only' : '')}
      >
        {label}
      </label>
      <div className={classNames('relative rounded-md shadow-sm', hideLabel ? '' : 'mt-1')}>
        <Field
          type={type}
          name={name}
          id={name}
          className={classNames(
            'block w-full rounded-xl focus:outline-none text-base md:text-lg shadow-none py-3 focus:shadow-lg transition duration-300',
            showError(touched, error)
              ? 'placeholder:text-red-500 border-red-500 focus:border-red-500 focus:ring-red-500 focus:shadow-red-500/20'
              : 'border-gray-200 focus:border-primary focus:ring-primary focus:shadow-primary/20'
          )}
          placeholder={placeholder}
          aria-invalid={showError(touched, error)}
          aria-describedby={`${name}-error`}
        />
      </div>
      {showError(touched, error) && (
        <p className="mt-2 text-sm text-red-500" id={`${name}-error`}>
          {error}
        </p>
      )}
    </div>
  )
}
