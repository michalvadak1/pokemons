import { RootState, useGetPokemonsQuery } from '../../store'
import React, { useState } from 'react'
import PokemonThumbnail from '../../components/pokemons/pokemon-thumbnail/PokemonThumbnail'
import { useSelector } from 'react-redux'
import pokemonLogo from '../../assets/pokemon_logo.png'
import GoToPokemonForm from '../../components/pokemons/go-to-pokemon-form/GoToPokemonForm'

const limit = 12

export default function Pokemons() {
  const { recentlyVisited, favourites } = useSelector((state: RootState) => state.app)
  const [page, setPage] = useState(0)

  const { data: pokemonsData, isFetching } = useGetPokemonsQuery({ limit, offset: limit * page })

  const previousHandler = () => {
    setPage(page - 1)
  }

  const nextHandler = () => {
    setPage(page + 1)
  }
  return (
    <div className="p-10 space-y-6">
      <img src={pokemonLogo} className="mx-auto h-48" alt="Pokemon logo" />
      <GoToPokemonForm />

      {!isFetching && (
        <div className="space-y-7">
          <div className="grid grid-cols-12 gap-4">
            {pokemonsData?.results.map((result, key) => {
              const url = result.url.split('/')
              const id = url[url.length - 2]

              return <PokemonThumbnail key={key} id={id} name={result.name} />
            })}
          </div>
          <nav
            className="flex justify-between items-center py-3 px-4 bg-white border-t border-gray-200 sm:px-6"
            aria-label="Pagination"
          >
            <div className="flex flex-1 justify-between sm:justify-end">
              <button
                type="button"
                className="inline-flex relative items-center py-2 px-4 text-sm font-medium text-gray-700 bg-white hover:bg-gray-50 rounded-md border border-gray-300"
                disabled={!pokemonsData?.previous}
                onClick={previousHandler}
              >
                Previous
              </button>
              <button
                type="button"
                className="inline-flex relative items-center py-2 px-4 ml-3 text-sm font-medium text-gray-700 bg-white hover:bg-gray-50 rounded-md border border-gray-300"
                disabled={!pokemonsData?.next}
                onClick={nextHandler}
              >
                Next
              </button>
            </div>
          </nav>
        </div>
      )}

      {favourites.length > 0 && (
        <div className="space-y-4 text-center">
          <h2 className="text-xl font-bold">Favourites</h2>
          <div className="grid grid-cols-12">
            {favourites?.map((result, key) => (
              <PokemonThumbnail key={key} id={result.id} name={result.name} />
            ))}
          </div>
        </div>
      )}

      <div className="space-y-4 text-center">
        <h2 className="text-xl font-bold">Recently visited</h2>
        <div className="grid grid-cols-12">
          {recentlyVisited?.map((result, key) => (
            <PokemonThumbnail key={key} id={result.id} name={result.name} />
          ))}
        </div>
      </div>
    </div>
  )
}
