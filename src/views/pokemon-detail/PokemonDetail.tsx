import { useNavigate, useParams } from 'react-router-dom'
import {
  addFavourite,
  addRecentlyVisited,
  removeFavourite,
  RootState,
  useAppDispatch,
  useGetPokemonQuery,
} from '../../store'
import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { ArrowLeftIcon } from '@heroicons/react/solid'

export default function PokemonDetail() {
  const { favourites } = useSelector((state: RootState) => state.app)
  const { id } = useParams()
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const { data, isLoading } = useGetPokemonQuery({ id }, { skip: !id })

  useEffect(() => {
    if (data) dispatch(addRecentlyVisited({ name: data?.name, id }))
  }, [data])

  const isFavourite = () => {
    return favourites.some(value => value.id === id)
  }

  const toggleFavouriteHandler = () => {
    if (isFavourite()) {
      dispatch(removeFavourite({ name: data?.name, id }))
    } else {
      dispatch(addFavourite({ name: data?.name, id }))
    }
  }

  if (isLoading) {
    return <div>loading...</div>
  }

  if (!data) {
    return <div>Pokemon not found</div>
  }

  return (
    <div className="bg-white">
      <div className="pt-6">
        <nav className="px-4 sm:px-6 lg:px-8">
          <button type="button" className="flex gap-2 text-sm font-medium text-gray-900" onClick={() => navigate(-1)}>
            <ArrowLeftIcon className="w-5 h-5" />
            Back
          </button>
        </nav>

        <div className="mx-auto mt-6 max-w-2xl sm:px-6 lg:grid lg:grid-cols-3 lg:gap-x-8 lg:px-8 lg:max-w-7xl">
          <div className="hidden overflow-hidden rounded-lg lg:block aspect-w-3 aspect-h-4">
            <img
              src={data.sprites.front_default}
              alt="Pokemon default front"
              className="object-contain object-center w-full h-full"
            />
          </div>
          <div className="hidden lg:grid lg:grid-cols-1 lg:gap-y-8">
            <div className="overflow-hidden rounded-lg aspect-w-3 aspect-h-2">
              <img
                src={data.sprites.back_default}
                alt="Pokemon default back"
                className="object-contain object-center w-full h-full"
              />
            </div>
            <div className="overflow-hidden rounded-lg aspect-w-3 aspect-h-2">
              <img
                src={data.sprites.back_shiny}
                alt="Pokemon shiny back"
                className="object-contain object-center w-full h-full"
              />
            </div>
          </div>
          <div className="sm:overflow-hidden sm:rounded-lg aspect-w-4 aspect-h-5 lg:aspect-w-3 lg:aspect-h-4">
            <img
              src={data.sprites.front_shiny}
              alt="Pokemon shiny front"
              className="object-contain object-center w-full h-full"
            />
          </div>
        </div>

        <div className="py-10 px-4 mx-auto max-w-2xl sm:px-6 lg:grid lg:grid-cols-3 lg:grid-rows-[auto,auto,1fr] lg:gap-x-8 lg:py-16 lg:px-8 lg:max-w-7xl">
          <div className="lg:col-span-2 lg:pr-8 lg:border-r lg:border-gray-200">
            <h1 className="text-2xl font-extrabold tracking-tight text-gray-900 first-letter:uppercase sm:text-3xl">
              {data.name}
            </h1>
          </div>

          <div className="mt-4 lg:row-span-3 lg:mt-0">
            <button
              type="button"
              className="flex justify-center items-center py-3 px-8 mt-10 w-full text-base font-medium rounded-md border border-transparent focus:outline-none focus:ring-2 focus:ring-offset-2 text-secondary hover:bg-primary-dark focus:ring-primary bg-primary"
              onClick={toggleFavouriteHandler}
            >
              {isFavourite() ? 'Remove from favourites' : 'Add to favourites'}
            </button>
          </div>

          <div className="py-10 lg:col-span-2 lg:col-start-1 lg:pt-6 lg:pr-8 lg:border-r lg:border-gray-200">
            <div>
              <h3 className="sr-only">Description</h3>

              <div className="space-y-6">
                <p className="text-base text-gray-900">Height: {data.height} dm</p>
                <p className="text-base text-gray-900">Weight: {data.weight} hg</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
