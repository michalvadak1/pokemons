describe("Pokemon app", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("Visit Bulbasaur - successfully", () => {
    cy.contains("bulbasaur").click();
    cy.contains("bulbasaur").should("exist");
  });

  it("Go to Bulbasaur - successfully", () => {
    cy.get("#name").type("bulbasaur");
    cy.get("#go-to-pokemon-form-submit").click();
    cy.contains("bulbasaur").should("exist");
  });

  it("Go to Bulbasau - not found", () => {
    cy.get("#name").type("bulbasau");
    cy.get("#go-to-pokemon-form-submit").click();
    cy.contains("Pokemon not found").should("exist");
  });
});
